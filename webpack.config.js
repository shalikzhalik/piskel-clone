const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    landing: './src/landing/landing.js',
    app: './src/app.js',
  },
  output: {
    filename: '[name][hash].bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [{
        test: /\.js$/i,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/i,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
          },

        }, ],
      },
      {
        test: /\.(s?css)$/i,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader',
        ]
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts/',
          }
        }]
      },
    ],
  },
  devServer: {
    overlay: true,
  },
  optimization: {
    minimize: true,
  },
  plugins: [
    new HTMLWebpackPlugin({
      template: './index.html',
      filename: 'index.html',
      chunks: ['landing'],
      hash: true,
      minify: true,
    }),
    new HTMLWebpackPlugin({
      template: './app.html',
      filename: 'app.html',
      chunks: ['app'],
      hash: true,
      title: 'piskel clone app',
    }),
  ],
};
