import * as framesData from './framesData';
import canvasSize from '../tools/setGetScaleCanvas';
import getFPS from '../previewAnimation/getFPS';

export default function setAdditPropertToMainDataObj() {
  const mainObj = framesData.getMainFramesDataObj();
  const sizze = canvasSize.getScale();
  const fpps = getFPS();

  mainObj.size = sizze;
  mainObj.fps = fpps;
  return mainObj;
}
