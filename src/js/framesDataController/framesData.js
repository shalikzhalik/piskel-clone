let Data = [];
let hiddenFrames = [];

function getMainFramesDataObj() {
  return {
    Data,
    hiddenFrames,
  };
}

function presetFromMainDataObj(obj) {
  Data = [...obj.Data];
  hiddenFrames = [...obj.hiddenFrames];
}

function getData(partId) {
  if (arguments.length === 0) {
    return Data;
  }
  if (arguments.length !== 0 && Data[partId]) {
    return Data[partId];
  }
  return false;
}

function presetData(data) {
  Data = [];
  Data = [...data];
}

function getImages() {
  const Arr = [];
  Data.map(item => Arr.push(item.data));
  return Arr;
}

function getSelectedFrame() {
  let X;
  Data.forEach((item, i) => {
    if (item.selected === 1) X = i;
  });
  return X;
}

function setSelectedFrame(fr) {
  Data.forEach((item, i) => {
    if (fr == i) { item.selected = 1 } else { item.selected = 0; }
  });
}

function pushData(data, part, selected) {
  if (arguments.length === 0) {
    Data.push({
      data: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAALUlEQVRYR+3QQREAAAABQfqXFsNnFTizzXk99+MAAQIECBAgQIAAAQIECBAgMBo/ACHo7lH9AAAAAElFTkSuQmCC',
      part: `${Data.length}`,
      selected: 1,
      layers: [],
    });
    setSelectedFrame(Data.length - 1);
  }
  if (arguments.length === 1 || arguments.length === 2 || arguments.length === 3) {
    Data.push({
      data,
      part,
      selected,
      layers: [],

    });
    setSelectedFrame(Data.length - 1);
  }
}

function resetDataImg(datta, part) {
  Data[part].data = datta;
}

function deleteFrame(delF) {
  if (Data[delF]) {
    if (Data[delF].selected === 1) {
      setSelectedFrame(delF - 1);
    }
    Data.splice(delF, 1);

    Data.map((item, i) => {
      item.part = i;
      return true;
    });

    return Data;
  }
  return false;
}

function replaseFrame(idexRemovedFrame, newPozition) {
  if (Data[idexRemovedFrame] && Data[newPozition]) {
    const data = Data.splice(idexRemovedFrame, 1);
    Data.splice(newPozition, 0, ...data);
    Data.map((item, i) => {
      item.part = i;
    });
  }
  return false;
}

function insertCopyFrame(copyIndex) {
  if (Data[copyIndex]) {
    const data = JSON.parse(JSON.stringify(Data[copyIndex]));
    data.selected = 0;
    Data.splice(copyIndex + 1, 0, data);
    Data.map((item, i) => {
      item.part = i;
    });
  }
}

function updateOneLayer(frame, layer, data) {
  Data[frame].layers[layer] = data;
}

function addOneLayer() {
  Data.forEach((item, i) => {
    item.layers.push('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAALUlEQVRYR+3QQREAAAABQfqXFsNnFTizzXk99+MAAQIECBAgQIAAAQIECBAgMBo/ACHo7lH9AAAAAElFTkSuQmCC');
  });
}

function getOneLayer(frame, layer) {
  return Data[frame].layers[layer];
}

function deleteLayer(frame, layer) {
  const deleted = Data[frame].layers.splice(layer, 1);
  return deleted;
}

function layerUp(frame, layer) {
  const item = Data[frame].layers.splice(layer, 1);
  Data[frame].layers.splice(layer - 1, 0, ...item);
  return item;
}

function layerDown(frame, layer) {
  const item = Data[frame].layers.splice(layer, 1);
  Data[frame].layers.splice(layer + 1, 0, ...item);
  return item;
}

function frameHideToggle(frame) {
  let consist = false;
  hiddenFrames.forEach((item, i) => {
    if (item[0] === +frame) {
      hiddenFrames.splice(i, 1);
      consist = true;
    }
  });
  if (!consist) {
    hiddenFrames.push([frame]);
  }
}

function getHiddenFramesIndexes() {
  return hiddenFrames;
}
module.exports = {
  getData,
  getImages,
  pushData,
  resetDataImg,
  deleteFrame,
  replaseFrame,
  deleteLayer,
  getSelectedFrame,
  setSelectedFrame,
  insertCopyFrame,
  updateOneLayer,
  getOneLayer,
  addOneLayer,
  presetData,
  layerDown,
  layerUp,
  frameHideToggle,
  getHiddenFramesIndexes,
  getMainFramesDataObj,
  presetFromMainDataObj,
};
