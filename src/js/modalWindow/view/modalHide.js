export default function modalHide() {
  const modalWindoww = document.getElementById('modal-window');
  const body = document.getElementById('popup-body');
  const elems = document.querySelectorAll('.popup-body .card-tool');
  elems.forEach((item) => {
    body.removeChild(item);
  });
  modalWindoww.style.display = 'none';
}
