import modalShow from './view/modalShow';
import renderModalItems from './renderModalItems';

export default function renderMenu() {
  renderModalItems().then(modalShow());
}
