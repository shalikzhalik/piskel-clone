import modalHide from './view/modalHide';
import updateToolsState from './updateToolsState';

export default function renderMenu() {
  updateToolsState();
  modalHide();
}
