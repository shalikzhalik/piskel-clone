import toolsState from '../tools/toolsState';

export default function updateToolsState() {
  const inputs = document.querySelectorAll('.modal-input');
  inputs.forEach((item) => {
    const tool = item.id;
    const newKeyKode = item.value.charCodeAt(0);
    toolsState.updateKeyCode(tool, newKeyKode);
  });
  console.log(toolsState.getState());
}
