import _ from 'lodash';
import toolsState from '../tools/toolsState';
import presetTools from '../tools/presetTools';

export default async function renderModalItems() {
  let data = toolsState.getState();
  if (data.length === 0) {
    presetTools();
    data = toolsState.getState();
  }
  const popupBody = document.getElementById('popup-body');

  data.forEach((item) => {
    popupBody.insertAdjacentHTML('beforeEnd', `
    <div class="${item.tool} card-tool">
        <div class="modal-container">
           <p>${_.capitalize(_.lowerCase(item.tool))}</p>
           <input type="text" id="${item.tool}" class="modal-input" value="${String.fromCharCode(item.hotKey)}" maxlength="1">
        </div>
    </div>`);
  });
}
