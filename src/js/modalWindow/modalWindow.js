import '../../css/modalWindow.css';

import renderMenu from './renderMenu';
import submitModal from './submitModal';
import modalHide from './view/modalHide';

export default function modalWindow() {
  const btn = document.getElementById('hotkeys');
  const modalWindoww = document.getElementById('modal-window');
  const closeBtn = document.getElementById('close');
  const modalSubmit = document.getElementById('modal-submit');

  btn.addEventListener('click', renderMenu);
  closeBtn.addEventListener('click', modalHide);
  window.addEventListener('click', (e) => {
    if (e.target === modalWindoww) modalHide();
  });
  modalSubmit.addEventListener('click', submitModal);
}
