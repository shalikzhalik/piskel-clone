import getSelectedLayer from './util/getSelectedLayer';
import getNextLayer from './util/getNextLayer';
import updateDatapart from './view/updateDataPart';

import getPartSelectedlayer from './util/getPartSelectedLayer';
import mergeLayers from '../controllers/mergeLayers';
import * as framesData from '../framesDataController/framesData';

export default function() {
  const currItem = getSelectedLayer();
  const nextItem = getNextLayer();
  const liContainer = document.querySelector('.layer-management ul');

  const previewCanvases = document.querySelectorAll('.preview-canvas');
  const selectedLayer = getPartSelectedlayer();

  if (nextItem) {
    // update data and preview
    previewCanvases.forEach((item, frame) => {
      const ctxPreviewCanvas = item.getContext('2d');

      framesData.layerDown(frame, selectedLayer);
      mergeLayers(frame, ctxPreviewCanvas, item);
    });

    liContainer.insertBefore(nextItem, currItem);
    updateDatapart();
  }
}
