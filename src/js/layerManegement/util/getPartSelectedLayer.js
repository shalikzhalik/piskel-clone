export default function getPartSelectedLayer() {
  const liContainer = document.querySelectorAll('.layer-management li');
  let selected = false;
  liContainer.forEach((item) => {
    if (item.classList[1] === 'selected-layer') {
      selected = item.dataset.part;
    }
  });
  return +selected;
}
