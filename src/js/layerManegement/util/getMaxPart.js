export default function() {
  const liContainer = document.querySelectorAll('.layer-management li');
  let maxPart = 0;
  liContainer.forEach((item) => {
    if (item.dataset.part > maxPart) {
      maxPart = item.dataset.part;
    }
  });
  return +maxPart;
}
