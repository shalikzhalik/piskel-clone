export default function() {
  const liContainer = document.querySelectorAll('.layer-management li');
  let selected = false;
  liContainer.forEach((item, i) => {
    if (item.classList[1] === 'selected-layer') {
      selected = i;
    }
  });
  return +selected;
}
