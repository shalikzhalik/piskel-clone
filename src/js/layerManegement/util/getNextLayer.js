import getPartSelectedLayer from './getPartSelectedLayer';

export default function() {
  const liContainer = document.querySelectorAll('.layer-management li');
  const searchedNext = getPartSelectedLayer() + 1;

  if (liContainer[searchedNext]) {
    return liContainer[searchedNext];
  }
}
