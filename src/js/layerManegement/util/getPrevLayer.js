import getPartSelectedLayer from './getPartSelectedLayer';

export default function() {
  const liContainer = document.querySelectorAll('.layer-management li');
  const searchedPrev = getPartSelectedLayer() - 1;

  if (liContainer[searchedPrev]) {
    return liContainer[searchedPrev];
  }
}
