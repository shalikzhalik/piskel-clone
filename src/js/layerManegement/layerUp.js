import getSelectedLayer from './util/getSelectedLayer';
import getPrevLayer from './util/getPrevLayer';
import updateDatapart from './view/updateDataPart';

import getPartSelectedlayer from './util/getPartSelectedLayer';
import mergeLayers from '../controllers/mergeLayers';
import * as framesData from '../framesDataController/framesData';

export default function() {
  const currItem = getSelectedLayer();
  const prevItem = getPrevLayer();
  const liContainer = document.querySelector('.layer-management ul');

  const previewCanvases = document.querySelectorAll('.preview-canvas');
  const selectedLayer = getPartSelectedlayer();

  if (prevItem) {
    previewCanvases.forEach((item, frame) => {
      const ctxPreviewCanvas = item.getContext('2d');

      framesData.layerUp(frame, selectedLayer);
      mergeLayers(frame, ctxPreviewCanvas, item);
    });


    liContainer.insertBefore(prevItem, currItem.nextSibling);
    updateDatapart();
  }
}
