import addLiLayer from './view/addLiLayer';
import * as framesData from '../framesDataController/framesData';

export default function createNewLayer() {
  addLiLayer();
  framesData.addOneLayer();
}
