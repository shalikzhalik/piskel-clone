import mergeLayers from '../controllers/mergeLayers';

export default function mergeLayersOfAllFrames() {
  const previewCanvases = document.querySelectorAll('.preview-canvas');


  previewCanvases.forEach((item, frame) => {
    const ctxPreviewCanvas = item.getContext('2d');
    mergeLayers(frame, ctxPreviewCanvas, item);
  });
}
