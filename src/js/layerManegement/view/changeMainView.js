import * as framesData from '../../framesDataController/framesData';
import getPartSelectedLayer from '../util/getPartSelectedLayer';
import putBase64ToCanvas from '../../controllers/putBase64ToCanvas';
import getCurrentCanvas from '../util/getCurrentCanvas';
import clearCanvas from '../../controllers/clearCanvas';

export default function changeMainView(e) {
  if (e.target.tagName === 'LI') {
    const currentFrame = framesData.getSelectedFrame();
    const selectedLayer = getPartSelectedLayer();
    const data = framesData.getOneLayer(currentFrame, selectedLayer);
    const ctx = getCurrentCanvas().getContext('2d');

    if (data) {
      clearCanvas(ctx);
      putBase64ToCanvas(ctx, data);
    } else {
      clearCanvas(ctx);
    }
  }
}
