export default function(selItem) {
  const listLi = document.querySelectorAll('.layer-management li');

  listLi.forEach((item, i) => {
    if (i === selItem) {
      item.classList.add('selected-layer');
    }
  });
}
