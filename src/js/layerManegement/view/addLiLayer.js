import getMaxPart from '../util/getMaxPart';

export default function() {
  const ulContainer = document.querySelector('.layer-management ul');
  const currMaxlayer = getMaxPart();

  ulContainer.insertAdjacentHTML('beforeEnd', `<li class="layer-${currMaxlayer + 1}" data-part="${currMaxlayer + 1}">Layer ${currMaxlayer + 2}</li> `);
}
