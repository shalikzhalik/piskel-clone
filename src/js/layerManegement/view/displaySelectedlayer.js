export default function displaySelectedLayer(e) {
  if (e.target.tagName === 'LI') {
    const listLi = document.querySelectorAll('.layer-management li');
    listLi.forEach((item) => {
      item.classList.remove('selected-layer');
    });
    e.target.classList.add('selected-layer');
  }
}
