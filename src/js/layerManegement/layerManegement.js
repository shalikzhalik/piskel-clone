import '../../css/layerManegement.css';

import displaySelectedlayer from './view/displaySelectedlayer';
import changeMainView from './view/changeMainView';

import createNewlayer from './createNewLayer';
import deleteLayer from './deleteLayer';
import layerUp from './layerUp';
import layerDown from './layerDown';

export default function() {
  const deleteLayerBtn = document.querySelector('.delete-layer');
  const insertLayerBtn = document.querySelector('.add-layer');
  const upLayerBtn = document.querySelector('.up-layer');
  const downLayerBtn = document.querySelector('.down-layer');
  const liContainer = document.querySelector('.layer-management ul');

  liContainer.addEventListener('click', displaySelectedlayer);
  liContainer.addEventListener('click', changeMainView);


  insertLayerBtn.addEventListener('click', createNewlayer);
  deleteLayerBtn.addEventListener('click', deleteLayer);
  upLayerBtn.addEventListener('click', layerUp);
  downLayerBtn.addEventListener('click', layerDown);
}
