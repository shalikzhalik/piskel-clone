import getSelectedLayer from './util/getSelectedLayer';
import updateDataPart from './view/updateDataPart';
import getPartSelectedlayer from './util/getPartSelectedLayer';
import getPozitionSelectedLayer from './util/getPozitionSelectedLayer';
import setSelectedlayer from './view/setSelectedLayer';
import clearCanvas from '../controllers/clearCanvas';

import putBase64ToCanvas from '../controllers/putBase64ToCanvas';
import mergeLayers from '../controllers/mergeLayers';
import * as framesData from '../framesDataController/framesData';

export default function() {
  const liContainer = document.querySelector('.layer-management ul');
  const previewCanvases = document.querySelectorAll('.preview-canvas');
  const selectedLayer = getPartSelectedlayer();
  const li = document.querySelectorAll('.layers-constainer li');
  if (li.length > 1) {
    // update data and preview
    previewCanvases.forEach((item, frame) => {
      const ctxPreviewCanvas = item.getContext('2d');

      framesData.deleteLayer(frame, selectedLayer);
      mergeLayers(frame, ctxPreviewCanvas);
    });
    // update main view
    const selectedFrame = framesData.getSelectedFrame();
    const mainCanvasCtx = document.querySelector('.mainCanvas').getContext('2d');
   
    const pozSelLayer = getPozitionSelectedLayer();
    liContainer.removeChild(getSelectedLayer());

    if (selectedLayer === 0) {
      setSelectedlayer(0);
      clearCanvas(mainCanvasCtx);
      putBase64ToCanvas(mainCanvasCtx, framesData.getOneLayer(selectedFrame, selectedLayer));
    } else {
      setSelectedlayer(pozSelLayer - 1);
      clearCanvas(mainCanvasCtx);
      putBase64ToCanvas(mainCanvasCtx, framesData.getOneLayer(selectedFrame, selectedLayer - 1));
    }
    updateDataPart();
  }
}
