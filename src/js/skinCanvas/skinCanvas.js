import getMousePosition from '../controllers/getMousePosition';
import clearCanvas from '../controllers/clearCanvas';

const mainContainerCanvas = document.querySelector('.block-container');

function setColor(x, y, r, g, b, a) {
  const canvas = document.querySelector('.skin-canvas');
  const ctx = canvas.getContext('2d');
  const p = ctx.createImageData(1, 1);
  p.data[0] = r;
  p.data[1] = g;
  p.data[2] = b;
  p.data[3] = a;

  ctx.putImageData(p, x, y);
}
export default function showHovered(e) {
  if (!e) return;
  const canvas = document.querySelector('.skin-canvas');
  const ctx = canvas.getContext('2d');
  const mPozition = getMousePosition(e);
  const x = mPozition[0];
  const y = mPozition[1];
  if (e.type === 'mousedown') {
    setColor(x, y, 255, 255, 255, 0);
  }
  if (e.type === 'mousemove') {
    clearCanvas(ctx);
    setColor(x, y, 219, 213, 213, 55);
  }
  if (e.type === 'mouseleave') {
    clearCanvas(ctx);
  }
}

mainContainerCanvas.addEventListener('mousedown', showHovered);
mainContainerCanvas.addEventListener('mousemove', showHovered);
mainContainerCanvas.addEventListener('mouseup', showHovered);
mainContainerCanvas.addEventListener('mouseleave', showHovered);
