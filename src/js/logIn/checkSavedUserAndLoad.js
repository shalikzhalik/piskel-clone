import addUser from './addUser';

export default function checkSavedUserAndLoad() {
  function a() {
    const googleAuth = window.gapi.auth2.getAuthInstance();
    if (googleAuth.isSignedIn.get()) {
      addUser();
    }
  }

  window.gapi.load('auth2', () => {
    window.gapi.auth2.init({
      client_id: '1090982764241-eabfu7319o882uajcu5hok3bo2dbouff.apps.googleusercontent.com',
      apiKey: 'AIzaSyChyPEuETrmN2L3XTN1l55qU268tVSumcA',
      scope: 'profile email  https://www.googleapis.com/auth/drive',
      discoveryDocs: ['https://www.googleapis.com/discovery/v1/apis/drive/v3/rest'],
    }).then(a, () => {});
  });
}
