export default function renderUser(userProfile) {
  const authBtn = document.getElementById('google-auth');
  const logOut = document.getElementById('google-log-out');
  const userAvatar = document.getElementById('user-data-avatar');
  const userName = document.getElementById('user-data-name');

  const userData = document.getElementById('user-data');
  userData.style.display = 'flex';
  authBtn.style.display = 'none';
  logOut.style.display = 'initial';
  userAvatar.src = `${userProfile.Paa}`;
  userName.innerText = `${userProfile.ig}`;
}
