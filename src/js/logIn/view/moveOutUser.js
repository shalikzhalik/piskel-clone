export default function moveOutUser() {
  const authBtn = document.getElementById('google-auth');
  const logOut = document.getElementById('google-log-out');
  const userData = document.getElementById('user-data');

  userData.style.display = 'none';
  logOut.style.display = 'none';
  authBtn.style.display = 'initial';
}
