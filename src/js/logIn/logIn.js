import addUser from './addUser';
import removeUser from './removeUser';

export default function onSignIn() {
  const authBtn = document.getElementById('google-auth');
  const logOut = document.getElementById('google-log-out');

  authBtn.addEventListener('click', addUser);
  logOut.addEventListener('click', removeUser);
}
