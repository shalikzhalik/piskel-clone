import moveOutUser from './view/moveOutUser';

export default function removeUser() {
  const googleAuth = window.gapi.auth2.getAuthInstance();
  googleAuth.signOut().then(moveOutUser);

}
