import renderUser from './view/renderUser';

export default function callRender(auth2) {
  const googleAuth = window.gapi.auth2.getAuthInstance();
  googleAuth.signIn({
    scope: 'profile email',
  }).then((user) => { renderUser(user.getBasicProfile()); }, () => {});
}
