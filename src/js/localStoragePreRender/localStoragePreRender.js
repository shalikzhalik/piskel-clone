import saveData from './saveData';
import loadAndRenderData from './loadAndRenderData';

export default function localStoragePreRender() {
  window.addEventListener('beforeunload', saveData);
  window.addEventListener('load', loadAndRenderData);
}
