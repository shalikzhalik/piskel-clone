import _ from 'lodash';

import putBase64ToCanvas from '../../controllers/putBase64ToCanvas';

function render(size, hiddenClass = '') {
  const ul = document.querySelector('.preview-list');
  const part = document.querySelectorAll('.preview-list li').length;

  ul.insertAdjacentHTML('beforeEnd', ` <li data-part="${part}" class="preview-tile" >
    <canvas width="${size}" height="${size}" data-part="${part}"  class="preview-canvas"></canvas>
  
    <button  class="frame-move">
        </button>
    <button class="frame-delete">
        </button>
    <button class="frame-copy">    </button>
  
    <button class="frame-number ${hiddenClass}"> ${part + 1}</button>
  </li>`);
}

function lastFrameCanvas() {
  const ul = document.querySelectorAll('.preview-list canvas');
  const last = ul.length - 1;
  return ul[last];
}

function getClass(hiddenFrames, i) {
  if (_.flatten(hiddenFrames).includes(i)) return 'hidden-frame';
  return '';
}
export default function renderFrames(data, size, hiddenFrames) {
  data.forEach((item, i) => {
    render(size, getClass(hiddenFrames, i));
    putBase64ToCanvas(lastFrameCanvas().getContext('2d'), item.data);
  });
}
