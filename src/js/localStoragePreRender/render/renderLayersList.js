import addLiLayer from '../../layerManegement/view/addLiLayer';

export default function renderLayersList(data) {
  const layersNumber = data[0].layers.length;
  for (let i = 1; i < layersNumber; i += 1) {
    addLiLayer();
  }
}
