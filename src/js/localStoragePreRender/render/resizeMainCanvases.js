export default function resizeMainCanvases(currentScale) {
  const middleCanvases = document.querySelectorAll('.block-container canvas');
  middleCanvases.forEach((item) => {
    item.width = currentScale;
    item.height = currentScale;
  });
}
