import * as framesData from '../../framesDataController/framesData';
import putBase64ToCanvas from '../../controllers/putBase64ToCanvas';
import resizeMainCanvases from './resizeMainCanvases';

export default function displayMainCanvas(size) {
  const mainCanvas = document.querySelector('.mainCanvas');
  const mainCtx = mainCanvas.getContext('2d');


  const selectedIndex = framesData.getSelectedFrame();
  const mainDataSelected = framesData.getOneLayer(selectedIndex, 0);

  resizeMainCanvases(size);
  if (mainDataSelected) {
    putBase64ToCanvas(mainCtx, mainDataSelected);
  }

}
