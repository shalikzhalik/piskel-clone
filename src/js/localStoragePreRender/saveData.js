import mainDataObj from '../framesDataController/setAdditPropertToMainDataObj';
import * as toolsState from '../tools/toolsState';

export default function saveData() {
  const allData = mainDataObj();
  const toolsStatee = toolsState.getState();

  if (allData.lenght === 0) return;
  localStorage.setItem('allData', JSON.stringify(allData));
  if (toolsStatee.length !== 0) {
    localStorage.setItem('toolsState', JSON.stringify(toolsStatee));
  }
}
