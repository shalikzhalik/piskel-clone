import renderFrames from './render/renderFrames';
import renderLayersList from './render/renderLayersList';
import displayMainCanvas from './render/displayMainCanvas';
import displaySelection from '../controllers/displaySelectionFrame';
import setFpsFromNumber from '../previewAnimation/setFpsFromNumber';
import setScale from '../tools/setGetScaleCanvas';

export default function renderWithData(datta) {
  let data;
  if (arguments.length === 0) {
    data = JSON.parse(localStorage.getItem('allData'));
  } else {
    data = datta;
  }
  const mainData = data.Data;
  const hiddeenFrames = data.hiddenFrames;
  const dataSize = data.size;
  const { fps } = data;


  setScale.setFromNumber(dataSize);
  setFpsFromNumber(fps);
  renderFrames(mainData, dataSize, hiddeenFrames);
  displayMainCanvas(dataSize);
  renderLayersList(mainData);
  displaySelection();
}
