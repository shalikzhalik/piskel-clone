import addNewFrame from '../framesManagement/addNewFrame';
import displaySelection from '../controllers/displaySelectionFrame';
import * as framesData from '../framesDataController/framesData';
import * as sizeCanvas from '../tools/setGetScaleCanvas';
import * as toolsState from '../tools/toolsState';
import renderWithData from './renderWithData';
import checkSavedUserAndLoad from '../logIn/checkSavedUserAndLoad';

export default function loadAndRenderData() {
  const data = JSON.parse(localStorage.getItem('allData'));
  const dataSize = data.size;
  const toolsSatee = JSON.parse(localStorage.getItem('toolsState'));
  checkSavedUserAndLoad();

  if (toolsSatee) {
    toolsState.setSavedToolsState(toolsSatee);
    if (toolsState.getSelectedTool()) {
      document.querySelector(`.${toolsState.getSelectedTool()}`).classList.add('selected-tool');
    }
  }
  if (data && data.Data.length > 0) {
    sizeCanvas.setFromNumber(+dataSize);
    framesData.presetFromMainDataObj(data);
    renderWithData();
  } else {
    addNewFrame();
    displaySelection();
  }
}
