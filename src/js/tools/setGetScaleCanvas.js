let currentScale = 32;

function setFromNumber(n) {
  currentScale = n;
}

function setScale() {
  const inputScale = document.getElementById('input-scale');
  const newVal = inputScale.value;
  if (newVal > 0 && newVal <= 360) {
    currentScale = newVal;
  }
}

function setScaleFromNumber(newScale) {
  currentScale = newScale;
}

function getScale() {
  return currentScale;
}

module.exports = {
  getScale,
  setScale,
  setFromNumber,
  setScaleFromNumber,
};
