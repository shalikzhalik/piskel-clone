import toRGB from './toolsFunction/util/hexToRGB';

const colorPick = document.querySelector('.firstColor');
const colorPick2 = document.querySelector('.secondColor');

let firstColor = [...toRGB(colorPick.value)];
let secondColor = [...toRGB(colorPick2.value)];

function changeColor(e) {
  const hex = this.value;
  const color = toRGB(hex);
  if (e.target.classList[0] === 'firstColor') {
    firstColor = color;
  }
  if (e.target.classList[0] === 'secondColor') {
    secondColor = color;
  }
  return color;
}

export default function getColors(e) {
  if (arguments.length === 0) {
    return [firstColor, secondColor];
  }
  const { which } = e;
  if (which === 1) {
    return firstColor;
  }
  if (which === 3) {
    return secondColor;
  }
  return [0, 0, 0];
}
colorPick.addEventListener('input', changeColor);
colorPick2.addEventListener('input', changeColor);
