import getCurrentCanvas from '../../layerManegement/util/getCurrentCanvas';

export default async function align(e) {
  const canvas = getCurrentCanvas();
  const ctx = canvas.getContext('2d');
  let left;
  let top;
  let rigth;
  let bottom;


  let flag1 = 1;
  for (let i = 0; i < canvas.width; i += 1) {
    const dataRow = ctx.getImageData(0, i, canvas.width, 1).data;
    if (flag1 && (dataRow.reduce((sum, curr) => { return sum + curr; }, 0))) {
      flag1 = 0;
      top = i;
    }
    if (dataRow.reduce((sum, curr) => { return sum + curr; }, 0)) {
      bottom = i;
    }
  }

  let flag2 = 1;
  for (let i = 0; i < canvas.height; i += 1) {
    const dataCol = ctx.getImageData(i, 0, 1, canvas.height).data;
    if (flag2 && (dataCol.reduce((sum, curr) => { return sum + curr; }, 0))) {
      flag2 = 0;
      left = i;
    }
    if (dataCol.reduce((sum, curr) => { return sum + curr; }, 0)) {
      rigth = i;
    }
  }
  if ((left + top + rigth + bottom) >= 0) {
    const putData = ctx.getImageData(left, top, rigth - left + 1, bottom - top + 1);
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.putImageData(putData, Math.floor((canvas.width - (rigth - left)) / 2), Math.floor((canvas.height - (bottom - top)) / 2));
  }
}
