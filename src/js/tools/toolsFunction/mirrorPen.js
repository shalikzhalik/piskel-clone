import drawLine from './util/drawLine';
import * as prevMousePozition from './util/prevMousePozition';

import getCurrentCanvas from '../../layerManegement/util/getCurrentCanvas';

export default function mirrorPen(x, y, e) {
  const canvas = getCurrentCanvas();
  const ctx = canvas.getContext('2d');
  const mousePozition = prevMousePozition.getPrevPoz();
  const prevX = mousePozition[0];
  const prevY = mousePozition[1];

  if (e.which === 2) return;

  if (e.type === 'mouseleave' || e.type === 'mouseup') {
    prevMousePozition.clearPrevPoz();
    return;
  }

  const pozX = (e.ctrlKey) ? x : canvas.width - x - 1;
  const pozY = (e.ctrlKey) ? canvas.height - y - 1 : y;
  const prevMirrorX = (e.ctrlKey) ? prevX : canvas.width - prevX - 1;
  const prevMirrorY = (e.ctrlKey) ? canvas.height - prevY - 1 : prevY;

  if (prevX && prevY || prevX === 0 || prevY === 0) {
    drawLine(prevX, prevY, x, y, e, ctx);
    drawLine(prevMirrorX, prevMirrorY, pozX, pozY, e, ctx);
  }
  prevMousePozition.updatePrevPoz(x, y);
}
