import _ from 'lodash';
import setPixel from './util/setPixel';
import canvasSize from '../setGetScaleCanvas';
import getCurrentCanvas from '../../layerManegement/util/getCurrentCanvas';


export default function sameColorBucket(x, y, e) {
  if (e.type === 'mousedown') {
    const canvas = getCurrentCanvas();
    const ctx = canvas.getContext('2d');

    const paintingStack = [{ x, y }];
    const canvSize = canvasSize.getScale() - 1;
    const replacedColor = ctx.getImageData(x, y, 1, 1).data;
    setPixel(ctx, x, y, e);
    const currentColor = ctx.getImageData(x, y, 1, 1).data;
    if (_.isEqual(replacedColor, currentColor)) return;

    // eslint-disable-next-line no-inner-declarations
    function updatePaintStack(xx, yy) {
      const leftX = xx - 1;
      const rigthX = xx + 1;
      const bottomY = yy + 1;
      const topY = yy - 1;
      if (_.isEqual(ctx.getImageData(leftX, yy, 1, 1).data, replacedColor) && leftX >= 0) {
        paintingStack.push({ x: leftX, y: yy });
      }
      if (_.isEqual(ctx.getImageData(xx, topY, 1, 1).data, replacedColor) && topY >= 0) {
        paintingStack.push({ x: xx, y: topY });
      }
      if (_.isEqual(ctx.getImageData(rigthX, yy, 1, 1).data, replacedColor) && rigthX <= canvSize) {
        paintingStack.push({ x: rigthX, y: yy });
      }
      if (_.isEqual(ctx.getImageData(xx, bottomY, 1, 1).data, replacedColor) && bottomY <= canvSize) {
        paintingStack.push({ x: xx, y: bottomY });
      }
    }

    while (paintingStack.length !== 0) {
      const pixel = paintingStack.pop();
      setPixel(ctx, pixel.x, pixel.y, e);
      updatePaintStack(pixel.x, pixel.y);
    }
  }
}
