import clearCanvas from '../../controllers/clearCanvas';
import getCurrentCanvas from '../../layerManegement/util/getCurrentCanvas';

let startX;
let startY;
let dataImage;
let movable = false;
export default function rectangle(x, y, e) {
  const canvas = getCurrentCanvas();
  const ctx = canvas.getContext('2d');

  if (e.which === 2) return;
  if (e.type === 'mousedown') {
    dataImage = ctx.getImageData(0, 0, canvas.width, canvas.height);
    startX = x;
    startY = y;
    movable = true;
  }

  if (e.type === 'mousemove') {
    clearCanvas(ctx);
    ctx.putImageData(dataImage, x - startX, y - startY);
  }
  if ((e.type === 'mouseup' || e.type === 'mouseleave') && movable) {
    movable = false;
    ctx.putImageData(dataImage, x - startX, y - startY);
  }
}
