import getCurrentCanvas from '../../layerManegement/util/getCurrentCanvas';

export default function setPixel(x, y, e) {
  const canvas = getCurrentCanvas();
  const ctx = canvas.getContext('2d');

  if (arguments.length === 0) return;
  if (e.which === 2 || e.which === 3) return;
  const p = ctx.createImageData(1, 1);

  p.data[0] = 0;
  p.data[1] = 0;
  p.data[2] = 0;
  p.data[3] = 0;

  ctx.putImageData(p, x, y);
}
