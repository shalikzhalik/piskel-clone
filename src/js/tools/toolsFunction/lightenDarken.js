import getCurrentCanvas from '../../layerManegement/util/getCurrentCanvas';

export default function lightenDarken(x, y, e) {
  const canvas = getCurrentCanvas();
  const ctx = canvas.getContext('2d');

  if (e.which === 2) return;
  if (e.type !== 'mousedown') return;
  const p = ctx.createImageData(1, 1);
  const Data = ctx.getImageData(x, y, 1, 1);
  let coeff = 1.1;
  if (e.which === 3) coeff *= 0.9;

  p.data[0] = Math.floor(Data.data[0] * coeff);
  p.data[1] = Math.floor(Data.data[1] * coeff);
  p.data[2] = Math.floor(Data.data[2] * coeff);
  p.data[3] = Math.floor(Data.data[3] * coeff);

  ctx.putImageData(p, x, y);
}
