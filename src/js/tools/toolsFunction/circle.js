import drawCircle from './util/drawCircle';
import clearCanvas from '../../controllers/clearCanvas';
import transfer from '../../controllers/transferBetwCanvases';
import getCurrentCanvas from '../../layerManegement/util/getCurrentCanvas';

const liveToolsCanvas = document.querySelector('.live-tools-canvas');
const ctx = liveToolsCanvas.getContext('2d');

let startX;
let startY;


export default function rectangle(x, y, e) {
  const toCanvas = getCurrentCanvas();
  const toCanvasCtx = toCanvas.getContext('2d');
  if (e.which === 2) return;
  if (e.type === 'mousedown') {
    startX = x;
    startY = y;
  }

  if (e.type === 'mousemove') {
    clearCanvas(ctx);
    drawCircle(startX, startY, x, y, e, ctx);
  }
  if (e.type === 'mouseup' || e.type === 'mouseleave') {
    transfer(liveToolsCanvas, toCanvas);
    clearCanvas(ctx, toCanvasCtx);
  }
}
