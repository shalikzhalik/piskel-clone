import getCurrentCanvas from '../../layerManegement/util/getCurrentCanvas';

export default function rotate() {
  return new Promise(((resolve, reject) => {
    const canvas = getCurrentCanvas();
    const ctx = canvas.getContext('2d');

    let myImage = new Image();
    myImage.src = canvas.toDataURL();
    myImage.onload = function load() {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.save();
      ctx.translate(canvas.width, 0);
      ctx.rotate(Math.PI / 2);
      ctx.drawImage(myImage, 0, 0);
      ctx.restore();
      myImage = null;
      resolve();
    };
  }));
}
