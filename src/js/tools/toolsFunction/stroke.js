import drawLine from './util/drawLine';
import clearCanvas from '../../controllers/clearCanvas';
import getCurrentCanvas from '../../layerManegement/util/getCurrentCanvas';
import transfer from '../../controllers/transferBetwCanvases';

const liveToolsCanvas = document.querySelector('.live-tools-canvas');
const ctx = liveToolsCanvas.getContext('2d');


let startX;
let startY;


export default function stroke(x, y, e) {
  const toCanvas = getCurrentCanvas();

  if (e.which === 2) return;
  if (e.type === 'mousedown') {
    startX = x;
    startY = y;
  }

  if (e.type === 'mousemove') {
    clearCanvas(ctx);
    drawLine(startX, startY, x, y, e, ctx);
  }
  if (e.type === 'mouseup' || e.type === 'mouseleave') {
    transfer(liveToolsCanvas, toCanvas);
    clearCanvas(ctx);
  }
}
