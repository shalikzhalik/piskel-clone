import getCurrentCanvas from '../../layerManegement/util/getCurrentCanvas';
import drawLine from './util/drawLine';
import * as prevMousePozition from './util/prevMousePozition';

export default function pen(x, y, e) {
  const canvas = getCurrentCanvas();
  const ctx = canvas.getContext('2d');
  const mousePozition = prevMousePozition.getPrevPoz();
  const prevX = mousePozition[0];
  const prevY = mousePozition[1];

  if (e.type === 'mouseleave' || e.type === 'mouseup') {
    prevMousePozition.clearPrevPoz();
    return;
  }
  if (e.which === 2) return;
  if (prevX && prevY || prevX === 0 || prevY === 0) {
    drawLine(prevX, prevY, x, y, e, ctx);
  }
  prevMousePozition.updatePrevPoz(x, y);
}
