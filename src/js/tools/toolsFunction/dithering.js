import getColors from '../getColors';
import getCurrentCanvas from '../../layerManegement/util/getCurrentCanvas';


export default function dithering(x, y, e) {
  const canvas = getCurrentCanvas();
  const ctx = canvas.getContext('2d');

  if (e.type === 'mouseleave') return;
  const which = e.which;
  if (which === 2) return;
  const p = ctx.createImageData(1, 1);
  const color = getColors();
  if (which === 3) {
    color.reverse();
  }
  const firstColor = color[0];
  const secondColor = color[1];
  let selected;

  if ((x + y) % 2 === 0) {
    selected = firstColor;
  } else {
    selected = secondColor;
  }


  p.data[0] = selected[0];
  p.data[1] = selected[1];
  p.data[2] = selected[2];
  p.data[3] = 255;

  ctx.putImageData(p, x, y);
}
