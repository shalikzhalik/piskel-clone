import getCurrentCanvas from '../../layerManegement/util/getCurrentCanvas';

export default async function flip(e) {
  const canvas = getCurrentCanvas();
  const ctx = canvas.getContext('2d');

  const part = ctx.getImageData(0, 0, canvas.width, canvas.height);
  const dataPart = part.data;
  const numPixels = canvas.width * canvas.height;
  for (let i = 0; i < numPixels; i += 1) {
    dataPart[i * 4] = 255 - dataPart[i * 4];
    dataPart[i * 4 + 1] = 255 - dataPart[i * 4 + 1];
    dataPart[i * 4 + 2] = 255 - dataPart[i * 4 + 2];
  }
  ctx.putImageData(part, 0, 0);
}
