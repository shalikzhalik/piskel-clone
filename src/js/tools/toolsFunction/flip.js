import getCurrentCanvas from '../../layerManegement/util/getCurrentCanvas';


export default async function flip(e) {
  const canvas = getCurrentCanvas();
  const ctx = canvas.getContext('2d');

  for (let i = 0; i < canvas.width / 2; i += 1) {
    const part = ctx.getImageData(i, 0, 1, canvas.height);
    const part2 = ctx.getImageData(canvas.width - 1 - i, 0, 1, canvas.height);
    ctx.putImageData(part, canvas.width - 1 - i, 0);
    ctx.putImageData(part2, i, 0);
  }
}
