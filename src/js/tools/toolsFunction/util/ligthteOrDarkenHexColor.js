export default function ColorLuminance(hex, lum) {
  let copyHEX = hex;
  const copyLum = lum || 0;
  copyHEX = String(hex).replace(/[^0-9a-f]/gi, '');
  if (hex.length < 6) {
    copyHEX = copyHEX[0] + copyHEX[0] + copyHEX[1] + copyHEX[1] + copyHEX[2] + copyHEX[2];
  }

  // convert to decimal and change luminosity
  let newHEX = '#';
  let c;
  let i;
  for (i = 0; i < 3; i += 1) {
    c = parseInt(copyHEX.substr(i * 2, 2), 16);
    c = Math.round(Math.min(Math.max(0, c + (c * copyLum)), 255)).toString(16);
    newHEX += (`00${c}`).substr(c.length);
  }

  return newHEX;
}
