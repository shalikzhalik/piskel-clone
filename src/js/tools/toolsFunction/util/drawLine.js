import setPixel from './setPixel';

export default function drawLine(startX, startY, x, y, e, ctx) {
  const deltaX = Math.abs(x - startX);
  const deltaY = Math.abs(y - startY);
  const signX = startX < x ? 1 : -1;
  const signY = startY < y ? 1 : -1;
  //
  let error = deltaX - deltaY;
  //
  setPixel(ctx, x, y, e);
  while (startX !== x || startY !== y) {
    setPixel(ctx, startX, startY, e);
    const error2 = error * 2;
    //
    if (error2 > -deltaY) {
      error -= deltaY;
      startX += signX;
    }
    if (error2 < deltaX) {
      error += deltaX;
      startY += signY;
    }
  }
}
