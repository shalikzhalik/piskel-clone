let prevX;
let prevY;

function getPrevPoz() {
  return [prevX, prevY];
}

function clearPrevPoz() {
  prevX = null;
  prevY = null;
}

function updatePrevPoz(x, y) {
  prevX = x;
  prevY = y;
}
module.exports = {
  getPrevPoz,
  clearPrevPoz,
  updatePrevPoz,
}
