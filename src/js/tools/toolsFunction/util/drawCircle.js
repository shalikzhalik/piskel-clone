import setPixel from './setPixel';

export default function drawCircle(startX, startY, x, y, e, ctx) {
  const radius = Math.sqrt(((x - startX) ** 2) + ((y - startY) ** 2));
  let x0 = 0;
  let y0 = radius;
  let gap = 0;
  let delta = (2 - 2 * radius);
  while (y0 >= 0) {
    setPixel(ctx, startX + x0, startY - y0, e);
    setPixel(ctx, startX - x0, startY - y0, e);
    setPixel(ctx, startX - x0, startY + y0, e);
    setPixel(ctx, startX + x0, startY + y0, e);
    gap = 2 * (delta + y0) - 1;
    if (delta < 0 && gap <= 0) {
      x0 += 1;
      delta += 2 * x0 + 1;
      continue;
    }
    if (delta > 0 && gap > 0) {
      y0 -= 1;
      delta -= 2 * y0 + 1;
      continue;
    }
    x0 += 1;
    delta += 2 * (x0 - y0);
    y0 -= 1;
  }
}
