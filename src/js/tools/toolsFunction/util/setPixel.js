import getColors from '../../getColors';

export default function setPixel(ctx, x, y, e) {
  const p = ctx.createImageData(1, 1);
  const color = getColors(e);

  p.data[0] = color[0];
  p.data[1] = color[1];
  p.data[2] = color[2];
  p.data[3] = 255;

  ctx.putImageData(p, x, y);
}
