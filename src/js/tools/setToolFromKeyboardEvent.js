import stateTools from './toolsState';
import dispalaySelectionTool from './displaySelectionTool';

export default function setToolFromKeyboardEvent(e) {
  const code = e.keyCode;
  if (code >= 65 && code <= 90) {
    stateTools.setSelectedToolOnKeyCode(code);
    const selected = stateTools.getSelectedTool();
    dispalaySelectionTool(selected);
  }
}
