import stateTools from './toolsState';
import dispalaySelectionTool from './displaySelectionTool';

export default function setToolFromUserMouseAction(e) {
  const tool = e.target.classList[0];

  stateTools.setSelectedTool(tool);
  dispalaySelectionTool(tool);
}
