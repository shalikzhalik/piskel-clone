import * as sizeCanvas from './setGetScaleCanvas';
import getMousePosition from '../controllers/getMousePosition';

export default function canvasInfo(e) {
  const size = sizeCanvas.getScale();
  const poz = getMousePosition(e);
  const sizeItem = document.querySelector('.canvas-size');
  const pozItem = document.querySelector('.mouse-position');
  const inputScale = document.getElementById('input-scale');

  inputScale.value = size;
  sizeItem.innerHTML = `[${size}x${size}]`;
  pozItem.innerHTML = `[${poz[0]}:${poz[1]}]`;
}
