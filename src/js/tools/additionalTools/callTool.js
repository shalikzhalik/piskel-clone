import updateViewAndData from '../../controllers/updateViewAndData';
import toolsState from '../toolsState';

import flip from '../toolsFunction/flip';
import rotate from '../toolsFunction/rotate';
import align from '../toolsFunction/align';
import inverse from '../toolsFunction/inverse';

function getTool(tool) {
  switch (tool) {
    case 'flip':
      return flip;
    case 'rotate':
      return rotate;
    case 'align':
      return align;
    case 'inverse':
      return inverse;
    default:
      return function() {};
  }
}
export default function additionalTools(e) {
  if (e.type === 'click') {
    const tool = e.target.classList[0];
    (function call() {
      getTool(tool)(e).then(() => { updateViewAndData(); });
    }(e));
  }
  if (e.type === 'keydown') {
    const toolCode = e.keyCode;
    const tool = toolsState.getToolOnKeyCode(toolCode);
    if (tool) {
      (function call() {
        getTool(tool)(e);
        updateViewAndData();
      }(e));
    }
  }
}
