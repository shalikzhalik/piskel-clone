export default function displaySelectionTool(select) {
  const allTools = document.querySelectorAll('.main-tools li');

  allTools.forEach((item, i) => {
    item.classList.remove('selected-tool');
    if (item.classList.contains(select)) {
      item.classList.add('selected-tool');
    }
  });
}
