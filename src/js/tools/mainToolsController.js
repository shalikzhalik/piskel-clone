import '../../css/toolBar.css';
import '../../css/scaleContainer.css';

import pen from './toolsFunction/pen';
import eraser from './toolsFunction/eraser';
import mirrorPen from './toolsFunction/mirrorPen';
import sameColorBucket from './toolsFunction/sameColorBucket';
import dithering from './toolsFunction/dithering';
import lightenDarken from './toolsFunction/lightenDarken';
import stroke from './toolsFunction/stroke';
import rectangle from './toolsFunction/rectangle';
import circle from './toolsFunction/circle';
import bucket from './toolsFunction/penBucket';
import move from './toolsFunction/move';
import shape from './toolsFunction/shapeTool/shape';

import stateTools from './toolsState';
import setToolFromUserMouseAction from './setToolFromUserMouseAction';
import setToolFromKeyboardEvent from './setToolFromKeyboardEvent';
import presetTools from './presetTools';

export default function(e) {
  let currentTool;
  let state = stateTools.getState();
  if (state.length === 0) {
    state = presetTools();
  }
  if (arguments.length === 1 && e.target.parentNode.classList[0] === 'main-tools') {
    setToolFromUserMouseAction(e);
  }
  if (arguments.length === 1 && e.type === 'keydown') {
    setToolFromKeyboardEvent(e);
  }


  if (arguments.length === 0) {
    currentTool = stateTools.getSelectedTool();
  }

  switch (currentTool) {
    case 'pen':
      return pen;
    case 'eraser':
      return eraser;
    case 'mirror-pen':
      return mirrorPen;
    case 'same-color-bucket':
      return sameColorBucket;
    case 'stroke':
      return stroke;
    case 'circle':
      return circle;
    case 'rectangle':
      return rectangle;
    case 'lighten':
      return lightenDarken;
    case 'dithering':
      return dithering;
    case 'bucket':
      return bucket;
    case 'move':
      return move;
    case 'shape-selection':
      return shape;
    default:
      return function() {};
  }
}
