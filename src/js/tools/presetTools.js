import toolsState from './toolsState';


export default function presetTools() {
  const keyCodeA = 65;
  const mainTools = document.querySelectorAll('.main-tools .tool');
  const additionalTools = document.querySelectorAll('.additional-tools .tool');

  let lastIndex;
  mainTools.forEach((item, i) => {
    const tool = item.classList[0];
    const selected = 0;
    const hotKey = keyCodeA + i;

    toolsState.addMainTool(tool, selected, hotKey);
    lastIndex = i;
  });

  additionalTools.forEach((item, i) => {
    const tool = item.classList[0];
    const hotKey = keyCodeA + i + lastIndex + 1;
    toolsState.addAdditionalTool(tool, hotKey);
  });
  return toolsState.getState();
}
