import getMousePosition from '../../controllers/getMousePosition';
import canvasInfo from '../canvasInfo';
import updateViewAndData from '../../controllers/updateViewAndData';
import getTool from '../mainToolsController';

let drawing = false;

export default function(e) {
  const mousePosition = getMousePosition(e);


  if (e.type === 'mousedown') {
    (function drawingStartFunc() {
      drawing = true;
      getTool()(mousePosition[0], mousePosition[1], e);
    }(e));
  }

  if (e.type === 'mousemove') {
    (function drawingFunc() {
      canvasInfo(e);
      if (drawing) {
        getTool()(mousePosition[0], mousePosition[1], e);
      }
    }(e));
  }

  if (e.type === 'mouseup' || e.type === 'mouseleave') {
    (function drawingEndFuncAndCopyCanvas() {
      drawing = false;
      getTool()(mousePosition[0], mousePosition[1], e);
      updateViewAndData();
    }(e));
  }
}
