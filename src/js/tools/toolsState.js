let state = [];
let selectedTool;

function getState() {
  return state;
}

function setSavedToolsState(newState) {
  state = newState;
}

function addMainTool(tool, selected, hotKey) {
  const obj = {
    tool,
    selected,
    hotKey,
  };

  state.push(obj);
}

function addAdditionalTool(tool, hotKey) {
  const obj = {
    tool,
    hotKey,
  };

  state.push(obj);
}

function setSelectedTool(tool) {
  if (selectedTool) {
    selectedTool.selected = 0;
  }
  state.forEach((item, i) => {
    if (item.tool === tool) {
      item.selected = 1;
      selectedTool = item;
    }
  });
}

function setSelectedToolOnKeyCode(keyCode) {
  if (selectedTool) {
    selectedTool.selected = 0;
  }
  state.forEach((item, i) => {
    if (item.hotKey === keyCode) {
      item.selected = 1;
      selectedTool = item;
    }
  });
}

function getSelectedTool() {
  if (selectedTool) return selectedTool.tool;

  state.forEach((item) => {
    if (item.selected === 1) selectedTool = item;
  });
  if (selectedTool) {
    return selectedTool.tool;
  }
  return false;
}

function getToolOnKeyCode(keyCode) {
  let tool;
  state.forEach((item) => {
    if (item.hotKey === keyCode) tool = item.tool;
  });
  return tool;
}

function updateKeyCode(tool, newKeyKode) {
  state.forEach((item) => {
    if (item.tool === tool) {
      item.hotKey = newKeyKode;
    }
  })
}

module.exports = {
  setSavedToolsState,
  getState,
  addMainTool,
  addAdditionalTool,
  getSelectedTool,
  setSelectedTool,
  setSelectedToolOnKeyCode,
  getToolOnKeyCode,
  updateKeyCode,
}
