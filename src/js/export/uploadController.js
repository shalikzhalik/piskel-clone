import gifData from './functions/gifData';
import apngData from './functions/apngData';
import OwnFormatData from './functions/OwnFormatData';
import piskelData from './functions/piskelData';

import uploadToGoogleDrive from './uploadToGoogleDrive';
import createFileForUpload from './functions/createFileForUpload';


export default function uploadController(e) {
  const { id } = e.target;
  if (id === 'upload-piskel') {
    piskelData().then((newDataObj) => {
      const file = createFileForUpload(newDataObj, 'text/plain', 'piskel');
      uploadToGoogleDrive(file);
    });
  }
  if (id === 'upload-sipkel') {
    const file = createFileForUpload(OwnFormatData(), 'text/plain', 'sipkel');
    uploadToGoogleDrive(file);
  }
  if (id === 'upload-gif') {
    // const file = createFileForUpload(gifData(), 'image/gif', 'gif');
    // uploadToGoogleDrive(file);
    alert('sorry, don\'t work now');
  }
  if (id === 'upload-apng') {
    const file = createFileForUpload(apngData(), 'image/apng', 'apng');
    uploadToGoogleDrive(file);
  }
}
