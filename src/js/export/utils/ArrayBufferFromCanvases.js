import * as canvasSize from '../../tools/setGetScaleCanvas';

export default function() {
  const scale = canvasSize.getScale();
  const data = [];
  const canvases = document.querySelectorAll('.preview-canvas');

  function getData(item) {
    const ctx = item.getContext('2d');
    const partData = ctx.getImageData(0, 0, scale, scale).data.buffer;
    data.push(partData);
  }
  canvases.forEach(getData);
  return data;
}
