export default function uploadToGoogleDrive(form) {

  const accesToken = window.gapi.auth.getToken().access_token;

  fetch('https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart&fields=id', {
    method: 'POST',
    headers: new Headers({ Authorization: `Bearer ${accesToken}` }),
    body: form,
  }).then(res => res.json()).then(() => {
    alert('All rigth');
  }, () => {
    alert('Something wrong');
  });
}
