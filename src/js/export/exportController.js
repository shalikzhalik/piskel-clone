import saveController from './saveController';
import uploadController from './uploadController';

export default function() {
  const gifToLocal = document.getElementById('gif-local-fs');
  const apngToLocal = document.getElementById('apng-local-fs');
  const ownFormat = document.getElementById('save-sipkel');
  const savePiskel = document.getElementById('save-piskel');


  const uploadPiskel = document.getElementById('upload-piskel');
  const uploadSipkel = document.getElementById('upload-sipkel');
  const uploadGIF = document.getElementById('upload-gif');
  const uploadAPNG = document.getElementById('upload-apng');


  savePiskel.addEventListener('click', saveController);
  ownFormat.addEventListener('click', saveController);
  gifToLocal.addEventListener('click', saveController);
  apngToLocal.addEventListener('click', saveController);

  uploadPiskel.addEventListener('click', uploadController);
  uploadSipkel.addEventListener('click', uploadController);
  uploadGIF.addEventListener('click', uploadController);
  uploadAPNG.addEventListener('click', uploadController);
}
