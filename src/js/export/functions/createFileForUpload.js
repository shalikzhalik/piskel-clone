export default function createFileForUpload(fileData, type, format) {
  const metaData = {
    name: `file.${format}`,
    mimeType: type,
  };

  if (format === 'piskel' || format === 'sipkel') {
    fileData = JSON.stringify(fileData);
  }
  const file = new Blob([fileData], { type });

  const form = new FormData();
  form.append('metadata', new Blob([JSON.stringify(metaData)], { type: 'application/json' }));
  form.append('file', file);

  return form;
}
