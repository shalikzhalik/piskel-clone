import UPNG from 'upng-js';

import fps from '../../previewAnimation/getFPS';
import * as canvasSize from '../../tools/setGetScaleCanvas';
import ArrayBufferData from '../utils/ArrayBufferFromCanvases';

export default function apngTolocalFs() {
  const scale = canvasSize.getScale();
  const imageData = UPNG.encode(ArrayBufferData(), scale, scale, 0, new Array(ArrayBufferData().length).fill(1000 / fps()));
  return imageData;
}
