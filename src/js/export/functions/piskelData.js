import _ from 'lodash';
import base64ToCanvasPromised from '../../controllers/base64ToCanvasPromised';
import mainDataObj from '../../framesDataController/setAdditPropertToMainDataObj';
import createCanvas from '../utils/createCanvas';

function addLayer(layer, layersArr, numFrames) {
  const layerObj = {
    name: `Layer${layer + 1}`,
    opacity: 1,
    frameCount: numFrames,
    chunks: [{
      layout: [],
      base64PNG: '1',
    }],
  };
  layersArr.push(layerObj);
}

async function addChunk(layersArr, layer, frame, data, canvas, size) {
  const ctx = canvas.getContext('2d');
  layersArr[layer].chunks[0].layout.push([frame]);
  return base64ToCanvasPromised(ctx, data[frame].layers[layer], frame * size);
}

export default function saveToPiskelFormat() {
  const mainData = mainDataObj();

  const { fps } = mainData;
  const { size } = mainData;
  const data = mainData.Data;
  const hiddenFrames = _.flatten(mainData.hiddenFrames);
  const numFrames = data.length;
  const numLayers = data[0].layers.length;
  const layersArr = [];
  const layerCanvases = [];
  const results = [];


  for (let layer = 0; layer < numLayers; layer += 1) {
    addLayer(layer, layersArr, numFrames);
    const canvas = createCanvas(size * numFrames, size);
    for (let frame = 0; frame < numFrames; frame += 1) {
      results.push(addChunk(layersArr, layer, frame, data, canvas, size));
    }
    layerCanvases.push(canvas);
  }

  return Promise.all(results).then(() => {

    layerCanvases.forEach((item, i) => {
      const datta = item.toDataURL();
      layersArr[i].chunks[0].base64PNG = datta;
    });


    layersArr.forEach((item, i) => {
      layersArr[i] = JSON.stringify(item);
    });

    const newData = {
      modelVersion: 2,
      piskel: {
        name: 'New Piskel',
        description: '',
        fps,
        height: size,
        width: size,
        layers: [...layersArr],
        hiddenFrames,
      },
    };
    return newData;
  });
}
