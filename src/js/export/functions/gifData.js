import GifENCODER from 'gif-encoder-2';
import fps from '../../previewAnimation/getFPS';
import canvasSize from '../../tools/setGetScaleCanvas';

export default function gifToLocal() {
  const frames = document.querySelectorAll('.preview-canvas');
  const size = canvasSize.getScale();


  const encoder = new GifENCODER(size, size);
  encoder.setDelay(1000 / fps);
  encoder.start();

  frames.forEach((item) => {
    const ctx = item.getContext('2d');
    encoder.addFrame(ctx);
  });

  encoder.finish();

  const buffer = encoder.out.getData();
  const b64encoded = btoa(String.fromCharCode.apply(null, buffer));
  const base64 = `data:image/jpg;base64,${b64encoded}`;
  return base64;
}
