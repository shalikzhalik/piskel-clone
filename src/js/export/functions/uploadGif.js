import axios from 'axios';

export default function uloadGifToGiphy(blob) {
  //const data = URL.createObjectURL(blob);
  const apiKey = 'I3tUL5AdvUYcgQiNaSoqkBeAy0JxB99U';
  const giphyAPI = `https://upload.giphy.com/v1/gifs?api_key=${apiKey}`;
  const giphyId = `https://api.giphy.com/v1/randomid?api_key=${apiKey}`;

  fetch(giphyId)
    .then((res) => {
      console.log(res);
      return res.json();
    })
    .then(({ data }) => data.random_id)
    .then((id) => {
      axios({
        method: 'POST',
        mode: 'no-cors',

        data: {
          file: blob,
        },
        url: `${giphyAPI}&id=${id}`,

      }).then((response) => {
        console.log(response);
      })
    })
    .catch((err) => { console.log(err); });

  /*
  axios.post(giphyAPI, {
      method: 'POST',
    })
    .then(function(response) {
      console.log(response);
    })
    .catch(function(error) {
      console.log(error);
    });
    */
  /*
    fetch(giphyAPI, { method: 'POST' })
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        console.log(json);
      })
      .catch(err => console.log(err));
  */

}
