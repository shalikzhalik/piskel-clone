import download from 'downloadjs';

import gifData from './functions/gifData';
import apngData from './functions/apngData';
import OwnFormatData from './functions/OwnFormatData';
import piskelData from './functions/piskelData';

export default function saveController(e) {
  if (e.target.classList[0] === 'save-piskel') {
    piskelData().then((piskelDataObj) => {
      download(JSON.stringify(piskelDataObj), 'data.piskel', 'piskel');
    });

  }
  if (e.target.classList[0] === 'save-sipkel') {
    download(JSON.stringify(OwnFormatData()), 'ownFormat.sipkel', 'sipkel');
  }
  if (e.target.classList[0] === 'apng-local-fs') {
    download(apngData(), 'newAPNG.apng', 'apng');
  }
  if (e.target.classList[0] === 'gif-local-fs') {
    download(gifData(), 'newGif.gif', 'gif');
  }
}
