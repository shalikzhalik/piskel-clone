import * as framesData from '../framesDataController/framesData';

export default function hideFrame(e) {
  if (e.target.classList[0] === 'frame-number') {
    e.target.classList.toggle('hidden-frame');
    framesData.frameHideToggle(+e.target.parentNode.dataset.part);
  }
}
