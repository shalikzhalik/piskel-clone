import Sortable from 'sortablejs';
import updateDataPartLi from '../controllers/updateDataPartLi';
import * as framesData from '../framesDataController/framesData';

export default function moveTool() {
  const ulLiContainer = document.querySelector('.preview-list');
  const LiElements = document.querySelectorAll('.preview-tile');

  const sortable = Sortable.create(ulLiContainer, {
    onEnd(event) {
      framesData.replaseFrame(event.oldDraggableIndex, event.newDraggableIndex);
      updateDataPartLi();
    },
  });
}
