import * as sizeCanvas from '../tools/setGetScaleCanvas';

export default function addNewFrame(afterThisNode) {
  const part = document.querySelectorAll('.preview-list li').length;
  const size = sizeCanvas.getScale();
  afterThisNode.insertAdjacentHTML('afterEnd', ` <li data-part="${part}" class="preview-tile" >
  
    <canvas width="${size}" height="${size}" data-part="${part}" class="preview-canvas"></canvas>
  
    <button  class="frame-move">
        </button>
    <button class="frame-delete">
        </button>
    <button class="frame-copy">    </button>
  
    <button class="frame-number"> ${part + 1}</button>
  
  </li>`);
}
