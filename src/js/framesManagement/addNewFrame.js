import * as framesData from '../framesDataController/framesData';
import clearCanvas from '../controllers/clearCanvas';
import displaySelection from '../controllers/displaySelectionFrame';
import * as sizeCanvas from '../tools/setGetScaleCanvas';

const ul = document.querySelector('.preview-list');
const canvas = document.querySelector('.mainCanvas');
const ctx = canvas.getContext('2d');

export default function addNewFrame() {
  const size = sizeCanvas.getScale();
  framesData.pushData();
  clearCanvas(ctx);
  const part = document.querySelectorAll('.preview-list li').length;
  ul.insertAdjacentHTML('beforeEnd', ` <li data-part="${part}" class="preview-tile" >
  
    <canvas width="${size}" height="${size}" data-part="${part}"  class="preview-canvas"></canvas>
  
    <button  class="frame-move">
        </button>
    <button class="frame-delete">
        </button>
    <button class="frame-copy">    </button>
  
    <button class="frame-number"> ${part + 1}</button>
  
  </li>`);
  displaySelection();
}
