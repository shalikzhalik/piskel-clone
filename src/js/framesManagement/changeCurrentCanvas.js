import clearCanvas from '../controllers/clearCanvas';
import displaySelection from '../controllers/displaySelectionFrame';
import putBase64ToCanvas from '../controllers/putBase64ToCanvas';
import * as framesData from '../framesDataController/framesData';
import getPozitionSelectedLayer from '../layerManegement/util/getPozitionSelectedLayer';

const canvas = document.querySelector('.mainCanvas');
const ctx = canvas.getContext('2d');

export default function changeCurrentCanvas(e) {
  const frame = +e.target.dataset.part;
  if (e.target.classList[0] === 'preview-canvas') {
    clearCanvas(ctx);

    framesData.setSelectedFrame(frame);
    const layer = getPozitionSelectedLayer();
    const dataMainLayer = framesData.getOneLayer(frame, layer);
    if (dataMainLayer) {
      putBase64ToCanvas(ctx, dataMainLayer);
    }


    displaySelection();
  }
}
