import * as framesData from '../framesDataController/framesData';
import clearCanvas from '../controllers/clearCanvas';
import insertNewFrame from './insertNewFrame';
import copyToFromCanvas from '../controllers/copyToFromCanvas';
import updateDataPartLi from '../controllers/updateDataPartLi';
import displaySelection from '../controllers/displaySelectionFrame';

const ul = document.querySelector('.preview-list');
const canvas = document.querySelector('.mainCanvas');
const ctx = canvas.getContext('2d');


export default function deleteOrCopyFrame(e) {
  let previewCanvases = document.querySelectorAll('.preview-list canvas');
  const eventItem = +e.target.parentNode.dataset.part;

  if (e.target.classList[0] === 'frame-delete' && +e.target.parentNode.dataset.part === 0) {
    const li = document.querySelectorAll('.preview-list li');
    const selected = framesData.getSelectedFrame();

    if (li.length === 1) {
      ul.removeChild(e.target.parentNode);
      clearCanvas(ctx);
      framesData.deleteFrame(0);
    }
    if (li.length !== 1 && selected === eventItem) {
      ul.removeChild(e.target.parentNode);
      framesData.deleteFrame(0);
      framesData.setSelectedFrame(0);
      displaySelection();
      updateDataPartLi();
    }
    if (li.length !== 1 && selected !== eventItem) {
      ul.removeChild(e.target.parentNode);
      framesData.deleteFrame(0);
      updateDataPartLi();
    }
    return;
  }


  if (e.target.classList[0] === 'frame-delete' && +framesData.getData(eventItem).part !== 0) {
    if (framesData.getSelectedFrame() == eventItem) {
      clearCanvas(ctx);
      copyToFromCanvas(canvas, previewCanvases[eventItem - 1]);
    }
    ul.removeChild(e.target.parentNode);
    framesData.deleteFrame(eventItem);
    displaySelection();
    updateDataPartLi();
  }
  if (e.target.classList[0] === 'frame-copy') {
    framesData.insertCopyFrame(eventItem);
    insertNewFrame(e.target.parentNode);
    updateDataPartLi();
    previewCanvases = document.querySelectorAll('.preview-list canvas');
    copyToFromCanvas(previewCanvases[eventItem + 1], previewCanvases[eventItem]);
  }
}
