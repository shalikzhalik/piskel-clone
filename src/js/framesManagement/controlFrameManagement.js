import '../../css/framesManegement.css';
import deleteOrCopyFrame from './deleteOrCopyFrame';
import changeCurrentCanvas from './changeCurrentCanvas';
import addNewFrame from './addNewFrame';
import moveTool from './moveTool';
import hideFrame from './hideFrame';

export default function() {
  const ul = document.querySelector('.preview-list');
  const addFrame = document.querySelector('.add-new-frame');

  ul.addEventListener('click', hideFrame);
  ul.addEventListener('click', changeCurrentCanvas);
  ul.addEventListener('click', deleteOrCopyFrame);
  addFrame.addEventListener('click', addNewFrame);
  moveTool();
}
