import _ from 'lodash';

import '../../css/preview.css';
import * as framesData from '../framesDataController/framesData';
import changeFps from './setFPS';
import fps from './getFPS';


const fullScreen = document.querySelector('.full-screen');

function toFullScreen() {
  document.querySelector('.preview-block').requestFullscreen();
}
const previewAnimation = document.querySelector('.img-preview-block');
const range = document.querySelector('.fps-range');

let currentPictire = 0;

function nextPicture() {
  const imgData = framesData.getData();
  const hiddenFrames = framesData.getHiddenFramesIndexes();

  if (imgData.length <= hiddenFrames.length) {
    return '';
  }
  if (_.flatten(hiddenFrames).includes(currentPictire)) {
    currentPictire += 1;
    return nextPicture();
  }


  if (imgData[currentPictire] && (currentPictire < imgData.length - 1)) {
    currentPictire += 1;
    return imgData[currentPictire - 1].data;
  }
  currentPictire = 0;
  if (_.flatten(hiddenFrames).includes(imgData.length - 1)) {
    return nextPicture();
  }
  return imgData[imgData.length - 1].data;
}

export default function animationStart() {
  setTimeout(() => {
    requestAnimationFrame(animationStart);
    previewAnimation.style.background = `url('${nextPicture()}')`;
    previewAnimation.style.backgroundSize = 'cover';
    previewAnimation.style.imageRendering = 'pixelated';
  }, 1000 / fps());
}

range.addEventListener('input', changeFps);
fullScreen.addEventListener('click', toFullScreen);
