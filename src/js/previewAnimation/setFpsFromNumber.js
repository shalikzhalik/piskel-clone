export default function setFpsFromNumber(number) {
  const fpsValue = document.querySelector('.fps-value');
  const fpsRange = document.querySelector('.fps-range');

  fpsValue.innerHTML = number;
  fpsRange.value = number;
}
