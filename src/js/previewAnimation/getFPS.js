export default function() {
  const range = document.querySelector('.fps-range');
  const fps = range.value;
  return fps;
}
