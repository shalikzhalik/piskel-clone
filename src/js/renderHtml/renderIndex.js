export default function renderIndex() {
  const main = document.querySelector('main');
  main.insertAdjacentHTML('afterBegin',
    ` <div class="main-functionality">
  <a href="./app.html" class="home-main-button">Create sprite</a>
  <div class="landing-paintiong-tools">
      <ul>
          <li><b>Tools:</b></li>
          <li>Pen</li>
          <li>Color Select</li>
          <li>Eraser</li>
          <li>Paint bucket</li>
          <li>Same color bucket </li>
          <li>Stroke</li>
          <li>Rectangle</li>
          <li>Circle</li>
          <li>Lighten/darken</li>
          <li>Dithering</li>
          <li>Move</li>
          <li>Mirror pen</li>
          <li>Flip</li>
          <li>Rotate</li>
          <li>Align center</li>
          <li>Inverse colors</li>
      </ul>
  </div>
  <div class="landing-other-abilitys">
      <ul>
          <li> <b>Other features:</b> </li>
          <li>Preview animation</li>
          <li>Frames management</li>
          <li>Layers management</li>
          <li>Keyboard shortcuts</li>
          <li>Save user session</li>
          <li>Export to GIF, PNG...</li>
          <li>Compatibility with .piskel</li>
      </ul>
  </div>
</div>
<div class="main-screenshoot"></div>
<div class="main-example">
  <div class="wolf"> </div>
  <div class="octupus"></div>
</div>
<div class="main-author">
  <p>Created by <a href="https://github.com/zzsshalik"> zzsshalik</a> </p>
</div>
`);
}
