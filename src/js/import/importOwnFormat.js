import * as framesData from '../framesDataController/framesData';
import setScale from '../tools/setGetScaleCanvas';

export default function importOwnFormat(contents) {
  framesData.presetFromMainDataObj(contents);
  setScale.setFromNumber(contents.size);
}
