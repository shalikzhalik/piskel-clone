export default function deleteLayers() {
  const layers = document.querySelectorAll('.layers-constainer li');
  layers.forEach((item, i) => {
    if (i !== 0) {
      item.parentNode.removeChild(item);
    }
  });
}
