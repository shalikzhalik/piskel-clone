export default function deleteFrames() {
  const ulChilds = document.querySelectorAll('.preview-list li');
  ulChilds.forEach((item) => {
    item.parentNode.removeChild(item);
  });
}
