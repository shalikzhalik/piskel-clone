import base64ToCanvasPrimisedForImport from './utils/base64ToCanvasPrimisedForImport';
import createCanvas from '../export/utils/createCanvas';

async function createLongCanvas(frameCount, frameSize, data) {
  const longCanvas = createCanvas(frameSize * frameCount, frameSize);
  return base64ToCanvasPrimisedForImport(longCanvas, data, frameSize * frameCount, frameSize);
}

async function cutLayers(longCanvas, numFrames, frameSize) {
  const ctxLongCanvas = longCanvas.getContext('2d');
  const middleCanvas = createCanvas(frameSize, frameSize);
  const ctxMiddleCanvas = middleCanvas.getContext('2d');

  const layerFrames = [];

  for (let i = 0; i < numFrames; i += 1) {
    const arrayBuffer = ctxLongCanvas.getImageData(frameSize * i, 0, frameSize, frameSize);
    ctxMiddleCanvas.clearRect(0, 0, frameSize, frameSize);
    ctxMiddleCanvas.putImageData(arrayBuffer, 0, 0);
    layerFrames.push(middleCanvas.toDataURL());
  }
  return layerFrames;
}

function pushFramesObjects(Data, numFrames) {
  for (let i = 0; i < numFrames; i += 1) {
    if (i === 0) {
      Data.push({
        data: '',
        part: i,
        selected: 1,
        layers: [],
      });
    } else {
      Data.push({
        data: '',
        part: i,
        selected: 0,
        layers: [],
      });
    }
  }
}

export default async function getFramesBase64FromLayersBase64(layers, frameSize) {
  const Data = [];
  const layersOpacity = {};
  const results = [];
  const results2 = [];
  const numFrames = JSON.parse(layers[0]).frameCount;

  layers.forEach((item) => {
    const dataLayer = JSON.parse(item);

    layersOpacity[`${dataLayer.name}`] = dataLayer.opacity;
    results.push(createLongCanvas(numFrames, frameSize, dataLayer.chunks[0].base64PNG));
  });

  return Promise.all(results).then((longCanvases) => {
    longCanvases.forEach((item, i) => {
      if (i === 0) {
        pushFramesObjects(Data, numFrames);
        results2.push(cutLayers(item, numFrames, frameSize));
      } else {
        results2.push(cutLayers(item, numFrames, frameSize));
      }
    });
    return results2;
  }).then(() => {
    return Promise.all(results2).then((cuttedFramesData) => {
      cuttedFramesData.forEach((framesOfLayer, layer) => {
        framesOfLayer.forEach((frameOfLayer, frame) => {
          Data[frame].layers.push(frameOfLayer);
        });
      });
      return {
        Data,
        layersOpacity,
      };
    });
  }).catch((err) => {});
}
