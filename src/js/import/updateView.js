import renderWithData from '../localStoragePreRender/renderWithData';
import saveData from '../localStoragePreRender/saveData';
import deleteLayers from './domFunctions/deleteLayers';
import deleteFrames from './domFunctions/deleteFrames';

export default function updateView(contents) {
  deleteLayers();
  deleteFrames();
  saveData();
  renderWithData(contents);
}
