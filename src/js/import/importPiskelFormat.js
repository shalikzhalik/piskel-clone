import * as framesData from '../framesDataController/framesData';
import getFramesBase64FromLayersBase64 from './getFramesBase64FromLayersBase64';


export default function importPiskelFormat(contents) {
  const { hiddenFrames } = contents.piskel;
  hiddenFrames.forEach((item, i) => {
    hiddenFrames[i] = [item];
  });

  return getFramesBase64FromLayersBase64(contents.piskel.layers, contents.piskel.width).then((layers) => {
    const newMainDataObj = {

      ...layers,
      fps: contents.piskel.fps,
      size: contents.piskel.width,
      description: contents.piskel.description,
      name: contents.piskel.name,
      hiddenFrames,

    };
    framesData.presetFromMainDataObj(newMainDataObj);

    return newMainDataObj;
  });
}
