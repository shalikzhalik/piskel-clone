export default async function Base64ToCanvasPromisedForImport(canvas, data) {
  const ctx = canvas.getContext('2d');

  return new Promise(((resolve, reject) => {
    const img = new Image();
    img.src = data;

    // eslint-disable-next-line func-names
    img.onload = function() {
      ctx.drawImage(img, 0, 0);
      resolve(canvas);
    };

    // eslint-disable-next-line func-names
    img.onerror = function() {
      reject(data);
    };
  }));
}
