import importOwnFormat from './importOwnFormat';
import importPiskelFormat from './importPiskelFormat';
import updateView from './updateView';
import mergeLayersOfAllFrames from '../layerManegement/mergeLayersOfAllFrames';

export default function importController() {
  const control = document.getElementById('import-own-format');

  const reader = new FileReader();
  reader.onload = (event) => {
    const contents = JSON.parse(event.target.result);
    if (contents.modelVersion === 2) {
      importPiskelFormat(contents).then((newObJ) => {
        updateView(newObJ);
        mergeLayersOfAllFrames();
      });
    } else {
      importOwnFormat(contents);
      updateView(contents);
    }
  };

  reader.onerror = () => {
    console.error('Error');
  };

  control.addEventListener('change', () => {
    reader.readAsText(control.files[0]);
  });
}
