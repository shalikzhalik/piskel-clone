import resetViewAndData from './controllers/resetViewAndData';
import animationStart from './previewAnimation/animationStart';
import prevDefContextmenu from './controllers/prevDefContextmenu';
import selectTool from './tools/mainToolsController';
import callTool from './tools/additionalTools/callTool';
import * as sizeCanvas from './tools/setGetScaleCanvas';
import drawing from './tools/drawing/mainDrawing';
import skinCanvas from './skinCanvas/skinCanvas';
import framesManagement from './framesManagement/controlFrameManagement';
import layerManegement from './layerManegement/layerManegement';
import logIn from './logIn/logIn';
import exportF from './export/exportController';
import importF from './import/importController';
import localStoragePreRender from './localStoragePreRender/localStoragePreRender';
import modalWindow from './modalWindow/modalWindow';
 
import $ from 'jquery';

const mainContainerCanvas = document.querySelector('.block-container');
const setScale = document.querySelector('.set-scale');
const mainToolsContainer = document.querySelector('.main-tools');
const additionalToolsContainer = document.querySelector('.additional-tools');


/*  Local storage and prerender */
localStoragePreRender();

/* frames management */
framesManagement();
layerManegement();
/* main canvas */
skinCanvas();
mainContainerCanvas.addEventListener('mousedown', drawing);
mainContainerCanvas.addEventListener('mousemove', drawing);
mainContainerCanvas.addEventListener('mouseup', drawing);
mainContainerCanvas.addEventListener('mouseleave', drawing);

document.querySelector('body').addEventListener('contextmenu', prevDefContextmenu);

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});

/* preview animation */
document.addEventListener('DOMContentLoaded', animationStart);

/* settings */

/* export */
exportF();
importF();
/* tools */
setScale.addEventListener('click', sizeCanvas.setScale);
setScale.addEventListener('click', resetViewAndData);

mainToolsContainer.addEventListener('click', selectTool);
window.addEventListener('keydown', selectTool);

additionalToolsContainer.addEventListener('click', callTool);
window.addEventListener('keydown', callTool);

/* log in */
logIn();

/* modal Window */
modalWindow();
