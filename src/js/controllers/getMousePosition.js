import * as sizeCanvas from '../tools/setGetScaleCanvas';

const block = document.querySelector('.block-container');
export default function getMousePosition(e) {
  return [Math.floor((e.offsetX) * sizeCanvas.getScale() / block.offsetWidth),
    Math.floor((e.offsetY) * sizeCanvas.getScale() / block.offsetHeight),
  ];

}
