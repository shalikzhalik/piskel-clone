import * as framesData from '../framesDataController/framesData';

export default function displaySelectionFrame() {
  const selected = framesData.getSelectedFrame();
  const ul = document.querySelectorAll('.preview-list li');
  ul.forEach((item) => {
    item.classList.remove('selected');
  });
  ul[selected].classList.add('selected');
}
