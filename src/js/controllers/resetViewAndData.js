import canvasInfo from '../tools/canvasInfo';
import * as framesData from '../framesDataController/framesData';
import * as sizeCanvas from '../tools/setGetScaleCanvas';

const canvas = document.querySelector('.mainCanvas');
const ctx = canvas.getContext('2d');

let previewCanvases = document.querySelectorAll('.preview-list canvas');


export default function resetViewAndData(e) {
  const currentScale = sizeCanvas.getScale();
  const middleCanvases = document.querySelectorAll('.block-container canvas');
  previewCanvases = document.querySelectorAll('.preview-list canvas');

  const mainCanvasData = ctx.getImageData(0, 0, currentScale, currentScale);
  canvas.width = currentScale;
  canvas.height = currentScale;
  ctx.putImageData(mainCanvasData, 0, 0);
  middleCanvases.forEach((item) => {
    const ctxx = item.getContext('2d');
    const data = ctxx.getImageData(0, 0, currentScale, currentScale);
    item.width = currentScale;
    item.height = currentScale;
    ctxx.putImageData(data, 0, 0);
  })


  previewCanvases.forEach((item, i) => {
    const ctxx = item.getContext('2d')
    const data = ctxx.getImageData(0, 0, currentScale, currentScale);
    item.width = currentScale;
    item.height = currentScale;
    ctxx.putImageData(data, 0, 0);
    framesData.resetDataImg(item.toDataURL(), i);
  });
  canvasInfo(e);
}
