import * as sizeCanvas from '../tools/setGetScaleCanvas';

export default function copyToFromCanvas(to, from) {
  const imageData = from.getContext('2d').getImageData(0, 0, sizeCanvas.getScale(), sizeCanvas.getScale());
  to.getContext('2d').putImageData(imageData, 0, 0);
}
