import clearCanvas from './clearCanvas';
import putBase64ToCanvas from './putBase64ToCanvas';

export default function transferBetwCanvases(liveToolsCanvas, toCanvas) {
  const newData = liveToolsCanvas.toDataURL();
  const toCanvasCtx = toCanvas.getContext('2d');
  const liveToolsCtx = liveToolsCanvas.getContext('2d');
  clearCanvas(liveToolsCtx);
  putBase64ToCanvas(toCanvasCtx, newData);
}
