import * as sizeCanvas from '../tools/setGetScaleCanvas';

export default async function Base64ToCanvasPromised(ctx, data, xGap = 0) {
  const scale = sizeCanvas.getScale();
  let dataArr;

  return new Promise(((resolve, reject) => {
    const img = new Image();
    img.src = data;

    // eslint-disable-next-line func-names
    img.onload = function () {
      ctx.drawImage(img, xGap, 0);
      dataArr = ctx.getImageData(0, 0, xGap + scale, scale);
      resolve(dataArr);
    };

    // eslint-disable-next-line func-names
    img.onerror = function () {
      reject(data);
    };
  }));
}
