import * as framesData from '../framesDataController/framesData';
import mergeLayers from './mergeLayers';
import getCurrentCanvas from '../layerManegement/util/getCurrentCanvas';
import getPartSelectedLayer from '../layerManegement/util/getPartSelectedLayer';

export default function updateViewAndData() {
  const previewCanvases = document.querySelectorAll('.preview-list canvas');


  if (previewCanvases[0]) {
    const currentCanvas = getCurrentCanvas();
    const previewCanvas = previewCanvases[framesData.getSelectedFrame()];
    const ctxPreviewCanvas = previewCanvas.getContext('2d');
    const frame = framesData.getSelectedFrame();

    // update layer data
    framesData.updateOneLayer(frame, getPartSelectedLayer(), currentCanvas.toDataURL());
    // merge layers
    mergeLayers(frame, ctxPreviewCanvas, previewCanvas);
  }
}
