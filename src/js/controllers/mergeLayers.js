import putBase64ToCanvas from './putBase64ToCanvas';
import base64ToCanvasPromised from './base64ToCanvasPromised';
import * as framesData from '../framesDataController/framesData';
import clearCanvas from './clearCanvas';

export default function mergeLayers(frame, ctxPreviewCanvas, previewCanvas) {
  const dataOfFrame = framesData.getData(frame);

  if (dataOfFrame.layers.length === 1) {
    clearCanvas(ctxPreviewCanvas);
    putBase64ToCanvas(ctxPreviewCanvas, dataOfFrame.layers[0]);
    framesData.resetDataImg(dataOfFrame.layers[0], frame);
  }
  clearCanvas(ctxPreviewCanvas);

  let i = 0;

  (function mmerge() {
    const { length } = dataOfFrame.layers;
    if (i === length) return;
    if (dataOfFrame.layers[i]) {
      base64ToCanvasPromised(ctxPreviewCanvas, dataOfFrame.layers[i]).then(() => {
        mmerge();
        framesData.resetDataImg(previewCanvas.toDataURL(), frame);
      }).catch(() => {});
    } else {
      i += 1;
      mmerge();
    }
    i += 1;
  }());
}
