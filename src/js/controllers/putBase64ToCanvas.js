import * as sizeCanvas from '../tools/setGetScaleCanvas';

let dataArr;

export default async function putBase64ToCanvas(ctx, data) {
  const scale = sizeCanvas.getScale();
  const image = new Image();
  image.src = data;
  image.onload = function() {
    ctx.drawImage(image, 0, 0);
    dataArr = ctx.getImageData(0, 0, scale, scale);
  }
  return dataArr;
}
