import * as sizeCanvas from '../tools/setGetScaleCanvas';

export default function clearCanvas(cctx) {
  cctx.save();
  cctx.setTransform(1, 0, 0, 1, 0, 0);
  cctx.clearRect(0, 0, sizeCanvas.getScale(), sizeCanvas.getScale());
  cctx.restore();
}
