export default function updateDataPartLi() {
  const elements = document.querySelectorAll('.preview-list li');
  const elementsOfParagraf = document.querySelectorAll('.frame-number');
  const elementsC = document.querySelectorAll('.preview-list canvas');

  elements.forEach((item, i) => {
    item.setAttribute('data-part', i);
    elementsOfParagraf[i].innerHTML = i + 1;
    elementsC[i].setAttribute('data-part', i);
  });
}
