import renderIndex from '../../js/renderHtml/renderIndex';

describe('render index html', () => {
  it('renderIndex should be instance of function', () => {
    expect(renderIndex).toBeInstanceOf(Function);
  });

  it('should be currectly render', () => {
    document.body.innerHTML = '<main></main>';
    renderIndex();

    expect(document.body.innerHTML).toMatchSnapshot();
  });

});
