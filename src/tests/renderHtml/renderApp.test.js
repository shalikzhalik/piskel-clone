import renderApp from '../../js/renderHtml/renderApp';

describe('render index html', () => {
  it('renderIndex should be instance of function', () => {
    expect(renderApp).toBeInstanceOf(Function);
  });

  it('should be currectly render', () => {
    document.body.innerHTML = '<body></body>';
    renderApp();

    expect(document.body.innerHTML).toMatchSnapshot();
  });

});
