import setGetScaleCanvas from '../../js/tools/setGetScaleCanvas';

describe('setGetScaleCanvas', () => {
  it('Sacale to be defuned', () => {
    expect(setGetScaleCanvas.getScale()).toBeDefined();
  });
  it('Should return new scale from number', () => {
    setGetScaleCanvas.setFromNumber(155);
    expect(setGetScaleCanvas.getScale()).toBe(155);
  });
  it('Should return new scale from class name 128-scale', () => {
    const e = {
      target: {
        classList: [
          '128-scale',
        ],
      },
    };
    setGetScaleCanvas.setScale(e);
    expect(setGetScaleCanvas.getScale()).toBe(128);
  });
  it('Should return new scale from class name 64-scale', () => {
    const e = {
      target: {
        classList: [
          '64-scale',
        ],
      },
    };
    setGetScaleCanvas.setScale(e);
    expect(setGetScaleCanvas.getScale()).toBe(64);
  });
  it('Should return new scale from class name 32-scale', () => {
    const e = {
      target: {
        classList: [
          '32-scale',
        ],
      },
    };
    setGetScaleCanvas.setScale(e);
    expect(setGetScaleCanvas.getScale()).toBe(32);
  });
});
