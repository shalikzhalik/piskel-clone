import RGBtoHEX from '../../../../js/tools/toolsFunction/util/RGBToHex';


describe('RGBtoHEX', () => {
  it(' 255,0,0 to equal #ff0000', () => {
    expect(RGBtoHEX(255, 0, 0)).toBe('#ff0000');
  });
  it('176,49,49 to equal #b03131', () => {
    expect(RGBtoHEX(176, 49, 49)).toBe('#b03131');
  });
  it('219, 164, 164 to equal #dba4a4', () => {
    expect(RGBtoHEX(219, 164, 164)).toBe('#dba4a4');
  });
});
