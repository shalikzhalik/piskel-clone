import HEXtoRGB from '../../../../js/tools/toolsFunction/util/hexToRGB';

describe('HEXtoRGB', () => {
  it('#ff0000 to equal [255, 0, 0]', () => {
    expect(HEXtoRGB('#ff0000')).toEqual([255, 0, 0]);
  });
  it('#b03131 to equal [176, 49, 49]', () => {
    expect(HEXtoRGB('#b03131')).toEqual([176, 49, 49]);
  });
  it('#dba4a4 to equal [219, 164, 164]', () => {
    expect(HEXtoRGB('#dba4a4')).toEqual([219, 164, 164]);
  });
});
