import jsdom from 'jsdom';
import Base64ToCanvasPromised from '../../js/controllers/base64ToCanvasPromised';


test('Base64ToCanvasPromised', () => {
  const { JSDOM } = jsdom;
  const data = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAALUlEQVRYR+3QQREAAAABQfqXFsNnFTizzXk99+MAAQIECBAgQIAAAQIECBAgMBo/ACHo7lH9AAAAAElFTkSuQmCC';
  const dom = new JSDOM('<body><canvas class="mainCanvas" width="32" height="32">No canvas</canvas></body>');
  const ctx = dom.window.document.querySelector('canvas').getContext('2d');

  return Base64ToCanvasPromised(ctx, data).then((newData) => {
    expect(newData).toBe('peanut butter');
  });
});
