import framesData from '../../js/framesDataController/framesData';

describe('framesData data push', () => {

  it('pushData without arguments should push default object', () => {
    const expected = {
      data: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAALUlEQVRYR+3QQREAAAABQfqXFsNnFTizzXk99+MAAQIECBAgQIAAAQIECBAgMBo/ACHo7lH9AAAAAElFTkSuQmCC',
      part: '0',
      selected: 1,
      layers: [],
    };
    framesData.pushData();
    expect(framesData.getData(0)).toEqual(expected);
  });
  it('pushData with one or more arguments should push filled object', () => {
    const expected = {
      data: 'newDataImg',
      part: 1,
      selected: 1,
      layers: [],
    };
    framesData.pushData('newDataImg', 1, 1);
    expect(framesData.getData(1)).toEqual(expected);
  });
});
