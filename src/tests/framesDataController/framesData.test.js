import framesData from '../../js/framesDataController/framesData';

describe('framesData d', () => {
  it('getMainFramesDataObj should return obj with hidden frames ad data', () => {
    expect(framesData.getMainFramesDataObj()).toEqual({
      Data: [],
      hiddenFrames: [],
    });
  });

  const frame1 = {
    data: 'img1',
    part: 0,
    selected: 1,
    layers: [],
  };
  const frame2 = {
    data: 'img2',
    part: 1,
    selected: 1,
    layers: [],
  };
  const obj = {
    Data: [frame1, frame2],
    hiddenFrames: [
      [1],
    ],
  };

  it('getMainFramesDataObj can preset data', () => {
    framesData.presetFromMainDataObj(obj);
    expect(framesData.getMainFramesDataObj()).toEqual(obj);
  });

  it('getData without arguments should return obj with all frames', () => {
    expect(framesData.getData()).toEqual([frame1, frame2]);
  });
  it('getData called with number argument should return object of frame', () => {
    expect(framesData.getData(1)).toEqual(frame2);
  });
  it('getImages should return array images of all frames', () => {
    expect(framesData.getImages()).toEqual(['img1', 'img2']);
  });
  it('getImages should return array images of all frames', () => {
    expect(framesData.getImages()).toEqual(['img1', 'img2']);
  });
  it('getSelectedFrame should return index of selected frame', () => {
    expect(framesData.getSelectedFrame()).toBe(1);
  });
  it('setSelectedFrame should set selected frame in oject of frame', () => {
    framesData.setSelectedFrame(0);
    expect(framesData.getSelectedFrame()).toBe(0);
  });
  let newFrame2 = {
    data: 'newImg2',
    part: 1,
    selected: 0,
    layers: [],
  };
  it('resetDataImg should update data property of data obj of frame', () => {
    framesData.resetDataImg('newImg2', 1);
    expect(framesData.getData()).toEqual([frame1, newFrame2]);
  });
  it('replaseFrame should replase frame with index to new pozition with new index and change part', () => {
    newFrame2 = {
      data: 'newImg2',
      part: 0,
      selected: 0,
      layers: [],
    };
    framesData.replaseFrame(1, 0);
    expect(framesData.getData()).toEqual([newFrame2, frame1]);
  });

  it('insertCopyFrame should add copy of frame with specified index to the next pozition ', () => {
    const expected = [{
        data: 'newImg2',
        part: 0,
        selected: 0,
        layers: [],
      },
      {
        data: 'img1',
        part: 1,
        selected: 1,
        layers: [],
      },
      {
        data: 'img1',
        part: 2,
        selected: 0,
        layers: [],
      },
    ];

    framesData.insertCopyFrame(1);

    expect(framesData.getData()).toEqual(expected);
  });

  it('deleteFrame should delete frame obj from array, update part indexes and move selected frame property', () => {
    const expected = [{
        data: 'newImg2',
        part: 0,
        selected: 1,
        layers: [],
      },
      {
        data: 'img1',
        part: 1,
        selected: 0,
        layers: [],
      }
    ];

    framesData.deleteFrame(1);
    expect(framesData.getData()).toEqual(expected);
  });
});
