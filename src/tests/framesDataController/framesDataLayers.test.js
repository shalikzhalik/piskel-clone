import framesData from '../../js/framesDataController/framesData';

describe('framesData layers', () => {
  framesData.pushData();
  it('addOneLayer should add default clean image of layer', () => {
    const expected = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAALUlEQVRYR+3QQREAAAABQfqXFsNnFTizzXk99+MAAQIECBAgQIAAAQIECBAgMBo/ACHo7lH9AAAAAElFTkSuQmCC';
    framesData.addOneLayer();
    expect(framesData.getOneLayer(0, 0)).toBe(expected);
  });

  const newLayerData0 = 'newLayer0';
  it('updateOneLayer should updata data layer img', () => {
    framesData.updateOneLayer(0, 0, newLayerData0);
    expect(framesData.getOneLayer(0, 0)).toBe(newLayerData0);
  });

  it('getOneLayer should return nth layer located on frame', () => {
    expect(framesData.getOneLayer(0, 0)).toBe(newLayerData0);
  });
  const newLayerData1 = 'newLayer1';
  it('layer up should raise the layer one position', () => {
    framesData.addOneLayer();
    framesData.updateOneLayer(0, 1, newLayerData1);
    framesData.layerUp(0, 1);

    expect(framesData.getOneLayer(0, 1)).toBe(newLayerData0);
    expect(framesData.getOneLayer(0, 0)).toBe(newLayerData1);
  });

  it('layer up should lower the layer one position', () => {
    framesData.layerDown(0, 0);

    expect(framesData.getOneLayer(0, 0)).toBe(newLayerData0);
    expect(framesData.getOneLayer(0, 1)).toBe(newLayerData1);
  });

});
