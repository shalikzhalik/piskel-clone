import framesData from '../../js/framesDataController/framesData';

describe('framesData hide frames', () => {

  it('getHiddenFramesIndexes should return empty array if no hidden frames', () => {
    expect(framesData.getHiddenFramesIndexes()).toEqual([]);
  });

  it('frameHideToggle should push array with index hidden frame ', () => {
    framesData.frameHideToggle(0);
    framesData.frameHideToggle(3);
    expect(framesData.getHiddenFramesIndexes()).toEqual([
      [0],
      [3],
    ]);
  });

  it('frameHideToggle should remove from array frame if that frame is hidden now', () => {
    framesData.frameHideToggle(0);
    expect(framesData.getHiddenFramesIndexes()).toEqual([
      [3],
    ]);
  });

});
